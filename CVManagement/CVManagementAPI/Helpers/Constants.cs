﻿using System.Text.RegularExpressions;


namespace CVManagementAPI.Helpers
{
    public class Constants
    {
        public static class SubjectMail
        {
            public const string REVIEW_CV_FAIL = "Thông báo kết quả sơ loại hồ sơ";
            public const string REVIEW_CV_PASS = REVIEW_CV_FAIL;

            public const string INTERVIEW_RES_FAIL = "Thông báo kết quả phỏng vấn";
            public const string INTERVIEW_RES_PASS = INTERVIEW_RES_FAIL;
            public const string INTERVIEW_RES_BACKUP = INTERVIEW_RES_FAIL;
        }
        public static class ContentMail
        {
            public const string REVIEW_CV_FAIL = @"<h1>Xin lỗi, bạn đã không được chọn</h1><p>Chúng tôi vừa hoàn tất quá trình đánh giá các ứng viên cho vị trí <strong>PositionName</strong>, và rất lấy làm tiếc phải thông báo rằng bạn không được chọn để tiếp tục quá trình phỏng vấn.</p><p>Chúng tôi đã nhận được rất nhiều hồ sơ ứng tuyển xuất sắc, và việc lựa chọn ứng viên đã trở nên vô cùng khó khăn. Tuy nhiên, chúng tôi vẫn rất ấn tượng với hồ sơ của bạn và mong muốn được tiếp tục hợp tác trong tương lai.</p><p>Chúng tôi chân thành cảm ơn bạn đã quan tâm và nộp hồ sơ ứng tuyển với chúng tôi. Chúc bạn sẽ sớm tìm được vị trí phù hợp với kinh nghiệm và kỹ năng của mình.</p><p><a target=""_blank"" rel=""noopener noreferrer nofollow"" class=""btn"" href=""https://example.com/job-openings"">Xem thêm các vị trí đang tuyển dụng</a></p>";

            public const string REVIEW_CV_PASS = @"<h1>Chúc mừng!</h1><p>Chúng tôi vui mừng thông báo rằng bạn đã được chọn để tham gia vào vòng phỏng vấn cho vị trí <strong>PositionName</strong> tại công ty của chúng tôi.</p><p>Ngày và giờ phỏng vấn của bạn:</p><p><strong>Ngày:</strong>DateOfInterview</p><p><strong>Giờ:</strong> TimeOfInterview</p><p><strong>Mức lương hỗ trợ:</strong><span>{Salary}</span></p><p>Vui lòng đến sớm 15 phút trước giờ phỏng vấn. Nếu bạn có bất kỳ câu hỏi hoặc thắc mắc nào, vui lòng liên hệ với chúng tôi.</p><p><a target=""_blank"" rel=""noopener noreferrer nofollow"" class=""btn"" href=""https://www.google.com/"">Xem thông tin chuẩn bị phỏng vấn</a></p>";

            public const string INTERVIEW_RES_FAIL = @"<h1>Xin lỗi, bạn đã không được chọn</h1><p>Chúng tôi vừa hoàn tất quá trình phỏng vấn ứng viên cho vị trí <strong>PositionName</strong>, và rất lấy làm tiếc phải thông báo rằng bạn không được chọn để tham gia vào công ty của chúng tôi.</p><p>Sau khi cân nhắc kỹ lưỡng, chúng tôi nhận thấy rằng bạn và công ty của chúng tôi chưa phù hợp với nhau về một số yếu tố như văn hóa, mục tiêu và kỳ vọng.</p><p>Đây là một quyết định khó khăn, nhưng chúng tôi tin rằng đây là lựa chọn tốt nhất cho cả hai bên.</p><p>Chúng tôi vẫn rất ấn tượng với hồ sơ và kỹ năng của bạn, và hy vọng sẽ có cơ hội hợp tác với bạn trong tương lai. Chúc bạn sẽ sớm tìm được một vị trí phù hợp với bản thân.</p><p><a target=""_blank"" rel=""noopener noreferrer nofollow"" class=""btn"" href=""https://example.com/job-openings"">Xem thêm các vị trí đang tuyển dụng</a></p>";

            public const string INTERVIEW_RES_PASS = @"<h1>Chúc mừng!</h1><p>Chúng tôi vui mừng thông báo rằng bạn đã được chọn để tham gia vào vị trí <strong>{PositionName}</strong> tại công ty của chúng tôi.</p><p><strong>Ngày bắt đầu công việc:</strong>{dateAcceptJob}</p><p><strong>Mức lương chính thức:</strong><span>{Salary}</span></p><p><strong>Địa chỉ văn phòng:</strong>{AddressDetail}</p><p>Chúng tôi rất mong được làm việc cùng bạn và trông đợi những đóng góp tuyệt vời của bạn cho công ty. Nếu bạn có bất kỳ câu hỏi hoặc thắc mắc nào, vui lòng liên hệ với chúng tôi.</p><p><a target=""_blank"" rel=""noopener noreferrer nofollow"" class=""btn"" href=""https://www.google.com/"">Xem thông tin về quy trình nhập việc</a></p>";

            public const string INTERVIEW_RES_BACKUP = @"<h1>Chúc mừng!</h1><p>Chúng tôi vui mừng thông báo rằng bạn đã được chọn để tham gia vào vị trí <strong>PositionName</strong> tại công ty của chúng tôi.</p><p>Tuy nhiên, do một số vấn đề nội bộ mà công ty đang giải quyết, việc bạn bắt đầu công việc sẽ phải tạm hoãn lại một thời gian.</p><p>Chúng tôi sẽ liên lạc với bạn trong vòng 2 tuần để cập nhật thông tin mới nhất và báo lại ngày bạn có thể bắt đầu làm việc.</p><p>Chúng tôi rất mong được làm việc cùng bạn và trông đợi những đóng góp tuyệt vời của bạn cho công ty. Nếu bạn có bất kỳ câu hỏi hoặc thắc mắc nào, vui lòng liên hệ với chúng tôi.</p><p><a target=""_blank"" rel=""noopener noreferrer nofollow"" class=""btn"" href=""https://example.com/contact-us"">Liên hệ với chúng tôi</a></p>";

        }

        public static class Gmail
        {
            public const string CC_DEFAULT = "lenguyenhaianh0808@gmail.com";
            public const string BCC_DEFAULT = "BCC";
        }
        public static class StatusCv
        {
            public const string REVIEW_CV_WAITING = "Đang chờ xem xét hồ sơ";
            public const string REVIEW_CV_FAIL = "Không đủ điều kiện";
            public const string REVIEW_CV_PASS = "Đủ điều kiện";
            public const string REFUSED_TO_BE_INTERVIEWED = "Từ chối phỏng vấn";
            public const string INTERVIEW_RES_FAIL = "Không đạt phỏng vấn";
            public const string INTERVIEW_RES_PASS = "Đạt phỏng vấn";
            public const string REFUSE_TO_ACCEPT_JOB = "Từ chối nhận việc";
            public const string GET_JOB = "Đã nhận việc";
            public const string INTERVIEW_RES_BACKUP = "Kết quả phỏng vấn dự phòng";
            public const string NOT_SENT = "NOT_SENT";
            public const string SENDING = "SENDING";
            public const string SENT = "SENT";
        }

        public static class EmailStatus
        {
            public const string NOT_SENT = "NOT_SENT";
            public const string SENDING = "SENDING";
            public const string SENT = "SENT";
        }
        public static class Address
        {
            public const string AddressDetail = "Tòa B phòng 27B5 ,Tòa Sun Square, 21 Lê Đức Thọ, Mỹ Đình 2, Nam Từ Liêm, Hà Nội.";
        }

        public static class Surname
        {
            public static List<string> list = new List<string>()
            {
                "Nguyễn","Trần","Lê","Phạm","Hoàng","Huỳnh","Vũ","Võ","Phan","Trương","Bùi","Đặng","Đỗ","Ngô","Hồ","Dương","Đinh","Lý","Ái", "An", "Anh", "Ao", "Ánh", "Ân", "Âu", "Âu Dương", "Ấu", "Bá", "Bạc", "Ban", "Bạch", "Bàn", "Bàng", "Bành", "Bảo", "Bế", "Bì", "Biện", "Bình", "Bồ", "Chriêng", "Ca", "Cà", "Cái", "Cai", "Cam", "Cảnh", "Cao", "Cáp", "Cát", "Cầm", "Cấn", "Chế", "Chiêm", "Chu", "Chắng", "Chung", "Chúng", "Chương", "Chử", "Cồ", "Cổ", "Công", "Cống", "Cung", "Cù", "Cự", "Dã", "Danh", "Diêm", "Diếp", "Doãn", "Diệp", "Du", "Duy", "Dư", "Đái", "Đan", "Đàm", "Đào", "Đăng", "Đắc", "Đầu", "Đậu", "Đèo", "Điêu", "Điền", "Điều", "Đinh", "Đình", "Đoái", "Đoàn", "Đoạn", "Đôn", "Đống", "Đồ", "Đồng", "Đổng", "Đới", "Đương", "Đường", "Đức", "Giả", "Giao", "Giang", "Giàng", "Giản", "Giảng", "Giáp", "Hưng", "H'", "H'ma", "H'nia", "Hầu", "Hà", "Hạ", "Hàn", "Hàng", "Hán", "Hề", "Hình", "Hoa", "Hoà", "Hoài", "Hoàng Phủ", "Hồng", "Hùng", "Hứa", "Hướng", "Hy", "Kinh", "Kông", "Kiểu", "Kha", "Khà", "Khai", "Khâu", "Khiếu", "Khoa", "Khổng", "Khu", "Khuất", "Khúc", "Khương", "Khưu", "Kiều", "Kim", "Ly", "Lý", "La", "Lã", "Lành", "Lãnh", "Lạc", "Lại", "Lăng", "Lâm", "Lầu", "Lèng", "Lều", "Liên", "Liệp", "Liêu", "Liễu", "Linh", "Loan", "Long", "Lò", "Lô", "Lỗ", "Lộ", "Lộc", "Luyện", "Lục", "Lù", "Lư", "Lương", "Lường", "Lưu", "Ma", "Mai", "Man", "Mang", "Mã", "Mê", "Mạc", "Mạch", "Mạnh", "Mâu", "Mậu", "Mầu", "Mẫn", "Minh", "Mộc", "Mông", "Mùa", "Mục", "Miêu", "Mễ", "Niê", "Ngạc", "Ngân", "Nghiêm", "Nghị", "Ngọ", "Ngọc", "Ngôn", "Ngũ", "Ngụy", "Nhan", "Nhâm", "Nhữ", "Ninh", "Nông", "Ong", "Ô", "Ông", "Phi", "Phí", "Phó", "Phong", "Phù", "Phú", "Phùng", "Phương", "Quản", "Quán", "Quang", "Quàng", "Quảng", "Quách", "Quế", "Quốc", "Quyền", "Sái", "Sâm", "Sầm", "Sơn", "Sử", "Sùng", "Sỳ", "Tán", "Tào", "Tạ", "Tăng", "Tấn", "Tất", "Tề", "Thang", "Thanh", "Thái", "Thành", "Thào", "Thạch", "Thân", "Thẩm", "Thập", "Thế", "Thi", "Thiều", "Thiệu", "Thịnh", "Thiềm", "Thoa", "Thôi", "Thóng", "Thục", "Tiêu", "Tiết", "Tiếp", "Tinh", "Tòng", "Tô", "Tôn", "Tôn Nữ", "Tôn Thất", "Tông", "Tống", "Trang", "Tráng", "Trác", "Trà", "Trâu", "Tri", "Trì", "Triệu", "Trình", "Trịnh", "Trung", "Trưng", "Tuấn", "Từ", "Tưởng", "Tướng", "Ty", "Uông", "Uân", "Ung", "Ưng", "Ứng", "Vàng", "Vâng", "Vạn", "Văn", "Văng", "Vi", "Vĩnh", "Viêm", "Viên", "Việt", "Vòng", "Vừ", "Vương", "Vưu", "Vu", "Xa", "Xung", "Y", "Yên", "Hầu", "Lương"
            };
            public static string RemoveDiacritics(string input)
            {
                input = Regex.Replace(input, "[àáảãạâầấẩẫậăằắẳẵặ]", "a");
                input = Regex.Replace(input, "[èéẻẽẹêềếểễệ]", "e");
                input = Regex.Replace(input, "[ìíỉĩị]", "i");
                input = Regex.Replace(input, "[òóỏõọôồốổỗộơờớởỡợ]", "o");
                input = Regex.Replace(input, "[ùúủũụưừứửữự]", "u");
                input = Regex.Replace(input, "[ỳýỷỹỵ]", "y");
                return input;
            }
            public static string StringRegex()
            {
                var result = "";
                for (int i = 0; i < list.Count; i++)
                {
                    result += list[i] + "|" + RemoveDiacritics(list[i]);
                    if (i < list.Count - 1)
                        result += "|";
                }
                return result;
            }
        }

        /// <summary>
        /// Thời gian hết hạn của session, file gửi lên từ client
        /// </summary>
        public const int ExpiredTime = 1;
    }
}
