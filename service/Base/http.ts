const port = {
    http: 'http://localhost:7081/api',
    https: 'https://localhost:5000',
};
export default port;
