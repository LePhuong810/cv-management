import CV_CONSTANT from '~/assets/js/constants/constants'
import { format } from 'date-fns';
function isArrayEmpty(arr) {
    var errors = []
    // Check if the input is an array
    if (!Array.isArray(arr)) {
        errors.push('Input is not an array.');
    }
    // Check if the array length is zero
    return arr.length === 0;
}
function validateCvInfoBeforeSendingMail(data) {
    console.log(data)
    var errors = []
    // For DateOfInterview dateOfInterview,
    if (data.status == CV_CONSTANT.REVIEW_CV_PASS) {
        if (data.dateOfInterview.trim() === '') {
            console.log(data.dateOfInterview)
            // Add an error message for DateOfInterview
            errors.push('Ngày phỏng vấn cần.');
        }
    }

    // For TimeOfInterview timeOfInterview
    if (data.status == CV_CONSTANT.REVIEW_CV_PASS) {
        if (data.timeOfInterview.trim() === '') {
            // Add an error message for TimeOfInterview
            errors.push('Thời gian phỏng vấn cần.');
        }
    }

    // For DateAcceptJob
    if (data.status == CV_CONSTANT.INTERVIEW_RES_PASS) {
        if (data.dateAcceptJob.trim() === '') {
            // Add an error message for DateAcceptJob
            errors.push('Ngày nhận việc cần.');
        }
    }
    return errors
}

/**
 * Handle yyyy-MM-dd format dates
 * @param {*} date
 */
function formatDateYYYYmmdd (date) {
    if (typeof date !== 'string') {
        return 'Invalid Date';
    }
    const dateObj = new Date(date);
    if (isNaN(dateObj.getTime())) {
        return 'Invalid Date';
    }
    return format(dateObj, 'yyyy-MM-dd');
};

export { isArrayEmpty, formatDateYYYYmmdd, validateCvInfoBeforeSendingMail }
